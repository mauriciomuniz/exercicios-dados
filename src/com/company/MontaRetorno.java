package com.company;

import java.util.ArrayList;
import java.util.List;

public class MontaRetorno {

    public String montaRetorno(List<Integer> numerosSorteados, int soma) {
        String retorno = "";

        String listString = numerosSorteados.toString();
        listString = listString.substring(1, listString.length() - 1);

        retorno = "Os numeros sorteados foram: "+ listString + " - A soma deles é: "+soma ;

        return retorno;
    }
}
