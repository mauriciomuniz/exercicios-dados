package com.company;

import java.util.Scanner;

public class IO {

    public static DadosInput solicitaInput() {

        DadosInput dadosInput = new DadosInput();

        System.out.println("Digite a quantidade de números a serem sorteados: ");
        Scanner entrada2 = new Scanner(System.in);

        int quantidadeNumerosPorSorteio = entrada2.nextInt();

        System.out.println("Digite a quantidade de sorteios: ");
        Scanner entrada1 = new Scanner(System.in);

        int quantidadeSorteios = entrada1.nextInt();

        System.out.println("Digite a quantidade de faces do Dado: ");
        Scanner entrada3 = new Scanner(System.in);

        int numeroFaces = entrada3.nextInt();

        dadosInput.setQuantidadeSorteios(quantidadeSorteios);
        dadosInput.setQuantidadeNumerosPorSorteio(quantidadeNumerosPorSorteio);
        dadosInput.setNumeroFaces(numeroFaces);

        return dadosInput;
    }
}
