package com.company;

import java.util.ArrayList;
import java.util.List;

public class Somador {

    public int soma(List<Integer> numerosSorteados) {
        int soma = 0;
        for (Integer numero : numerosSorteados) {
            soma += numero;
        }
        return soma;
    }
}
