package com.company;

import com.sun.xml.internal.ws.api.model.WSDLOperationMapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Orquestrador {
    Sorteador sorteador = new Sorteador();
    Somador somador = new Somador();
    MontaRetorno montaRetorno = new MontaRetorno();
    Impressora impressora = new Impressora();

    public void sorteio(DadosInput dadosInput) {

        List<Integer> numerosSorteados = new ArrayList<>();
        int soma = 0;
        String mensagemRetorno;

        for (int i = 0; i < dadosInput.getQuantidadeSorteios(); i++) {

            numerosSorteados = sorteador.sortear(dadosInput);
            soma = somador.soma(numerosSorteados);
            mensagemRetorno = montaRetorno.montaRetorno(numerosSorteados, soma);
            impressora.impressao(mensagemRetorno);
        }
    }

}
