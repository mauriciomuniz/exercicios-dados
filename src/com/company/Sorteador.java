package com.company;

import java.util.ArrayList;
import java.util.List;

public class Sorteador {

    public List<Integer> sortear(DadosInput dadosInput) {
        List<Integer> numerosSorteados = new ArrayList<>();

        for (int i = 0; i < dadosInput.getQuantidadeNumerosPorSorteio(); i++) {
            int numeroSorteado = (int) Math.ceil(Math.random() * dadosInput.getNumeroFaces());
            numerosSorteados.add(numeroSorteado);
        }
        return numerosSorteados;
    }
}
