package com.company;

public class DadosInput {

    public int quantidadeSorteios;

    public int quantidadeNumerosPorSorteio;

    public int numeroFaces;

    public int getQuantidadeSorteios() {
        return quantidadeSorteios;
    }

    public void setQuantidadeSorteios(int quantidadeSorteios) {
        this.quantidadeSorteios = quantidadeSorteios;
    }

    public int getQuantidadeNumerosPorSorteio() {
        return quantidadeNumerosPorSorteio;
    }

    public void setQuantidadeNumerosPorSorteio(int quantidadeNumerosPorSorteio) {
        this.quantidadeNumerosPorSorteio = quantidadeNumerosPorSorteio;
    }

    public int getNumeroFaces() {
        return numeroFaces;
    }

    public void setNumeroFaces(int numeroFaces) {
        this.numeroFaces = numeroFaces;
    }
}
