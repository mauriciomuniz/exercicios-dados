package com.company;

public class Main {

    public static void main(String[] args) {

        Orquestrador orquestrador = new Orquestrador();
        IO io = new IO();

        DadosInput dadosInput = io.solicitaInput();

        orquestrador.sorteio(dadosInput);
    }
}
